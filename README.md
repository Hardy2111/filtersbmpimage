# Графические фильтры (image_processor)

Консольное приложение,
позволяющее применять к изображениям различные фильтры,
аналогичные фильтрам в популярных графических редакторах.

## Поддерживаемый формат изображений

Формат входных и выходных файлов [BMP](http://en.wikipedia.org/wiki/BMP_file_format).
(24-битный BMP без сжатия и без таблицы цветов. Тип используемого `DIB header` - `BITMAPINFOHEADER`.)

Пример файла в нужном формате есть в статье на Википедии [в разделе "Example 1"](https://en.wikipedia.org/wiki/BMP_file_format#Example_1)
и в папке [examples](examples).


## Формат аргументов командной строки

Описание формата аргументов командной строки:

`{имя программы} {путь к входному файлу} {путь к выходному файлу}
[-{имя фильтра 1} [параметр фильтра 1] [параметр фильтра 2] ...]
[-{имя фильтра 2} [параметр фильтра 1] [параметр фильтра 2] ...] ...`

При запуске без аргументов программа выводит справку.

### Пример
`./image_processor input.bmp /tmp/output.bmp -crop 800 600 -gs -blur 0.5`

В этом примере
1. Загружается изображение из файла `input.bmp`
2. Обрезается до изображения с началом в верхнем левом углу и размером 800х600 пикселей
3. Переводится в оттенки серого
4. Применяется размытие с сигмой 0.5
5. Полученное изображение сохраняется в файл `/tmp/output.bmp`

## Фильтры

### Список базовых фильтров

#### Crop (-crop width height)
Обрезает изображение до заданных ширины и высоты. Используется верхняя левая часть изображения.

Если запрошенные ширина или высота превышают размеры исходного изображения, выдается доступная часть изображения.

#### Grayscale (-gs)
Преобразует изображение в оттенки серого.
#### Negative (-neg)
Преобразует изображение в негатив.
#### Sharpening (-sharp)
Повышение резкости. 
#### Edge Detection (-edge threshold)
Выделение границ. Изображение переводится в оттенки серого.
#### Gaussian Blur (-blur sigma)
Гауссовой размытие.