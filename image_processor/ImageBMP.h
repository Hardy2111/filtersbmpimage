#pragma once
#include "fstream"
#include "exception"
#include "vector"
#include <algorithm>
#include <cmath>
#include "iostream"

const size_t WEIGHT_OFFSET = 18;
const size_t PIXELS_OFFSET = 54;
const size_t SIZEBMP_OFFSET = 2;
const size_t BM_IN_SIZE_T = 19778;


class Pixel {
public:
    double Red;
    double Green;
    double Blue;


    Pixel& Reverse() {
        uint8_t max_equal = std::numeric_limits<uint8_t>::max();
        Red = max_equal - Red;
        Green = max_equal - Green;
        Blue = max_equal - Blue;
        return *this;
    }

    Pixel& operator=(uint8_t equal) {
        Red = Green = Blue = equal;
        return *this;
    };

    Pixel operator*(float equal) const {
        Pixel new_pixel = *this;
        new_pixel.Red *= equal;
        new_pixel.Blue *= equal;
        new_pixel.Green *= equal;
        return new_pixel;
    };

    Pixel operator-(Pixel other) {
        Pixel new_pixel = *this;
        new_pixel.Red = std::clamp(Red - other.Red, 0., 255.);
        new_pixel.Blue = std::clamp(Blue - other.Blue, 0., 255.);
        new_pixel.Green = std::clamp(Green - other.Green, 0., 255.);
        return new_pixel;
    };

    Pixel& operator+=(Pixel other) {
        Red += other.Red;
        Blue += other.Blue;
        Green += other.Green;
        return *this;
    };

    Pixel& operator+=(double equal) {
        Red += equal;
        Blue += equal;
        Green += equal;
        return *this;
    };

    Pixel operator+(const Pixel& other) const{
        Pixel new_pixel = *this;
        new_pixel.Red += other.Red;
        new_pixel.Blue += other.Blue;
        new_pixel.Green += other.Green;
        return new_pixel;
    };

    Pixel operator+(Pixel& other) const {
        Pixel new_pixel = *this;
        new_pixel.Red += other.Red;
        new_pixel.Blue += other.Blue;
        new_pixel.Green += other.Green;
        return new_pixel;
    };
};


class ImageException : public std::exception {
private:
    const char* except;

public:
    explicit ImageException(const char* data) : except(data){};
    const char* what() const throw() {
        return except;
    }
};

struct ImageBMP {
    uint32_t width_;
    uint32_t height_;
    uint16_t BM_;
    uint32_t sizebmp_;
    char header_[54];
    std::vector<std::vector<Pixel>> pixels_;
};


