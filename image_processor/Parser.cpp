#include "Parser.h"

Parser::Parser(int argc, char* argv[]) : arc(argc), arg(argv) {
    if (argc > 3) {
        istream = std::ifstream(argv[1]);
        ostream = std::ofstream(argv[2]);
    } else {
        throw FilterException("Not enough arguments!");
    }
    for (size_t i = 3; i < argc; ++i) {
        if (argv[i] == std::string("-crop")) {
            try {
                ++i;
                size_t width = std::stoi(argv[i]);
                ++i;
                size_t height = std::stoi(argv[i]);
                Crop* crop = new Crop(width, height);
                filters_.push_back(crop);
            } catch (const std::exception& ex) {
                throw FilterException("Bad height and width for -crop filter!");
            }
        } else if (argv[i] == std::string("-edge")) {
            try {
                ++i;
                double threshold = std::stod(argv[i]);
                EdgeDetection* edge = new EdgeDetection(threshold);
                filters_.push_back(edge);
            } catch (const std::exception& ex) {
                throw FilterException("Bad threshold for -edge filter!");
            }
        } else if (argv[i] == std::string("-gs")) {
            GrayScale* grayscale = new GrayScale;
            filters_.push_back(grayscale);
        } else if (argv[i] == std::string("-neg")) {
            Negative* negative = new Negative;
            filters_.push_back(negative);
        } else if (argv[i] == std::string("-sharp")) {
            Sharpening* sharpening = new Sharpening;
            filters_.push_back(sharpening);
        } else if (argv[i] == std::string("-copy")) {
            Copy* copy = new Copy;
            filters_.push_back(copy);
        } else if (argv[i] == std::string("-blur")) {
            try {
                ++i;
                double sigma = std::stod(argv[i]);
                Gaussian* gaussian = new Gaussian(sigma);
                filters_.push_back(gaussian);
            } catch (const std::exception& ex) {
                throw FilterException("Bad sigma for -blur filter!");
            }
        } else if (argv[i] == std::string("-cust")) {
            try {
                i++;
                size_t equal = std::stoi(argv[i]);
                CustomeFilter* custome = new CustomeFilter(equal);
                filters_.push_back(custome);
            } catch (const std::exception& ex) {
                throw FilterException("Bad equal for -cust filter!");
            }
        } else {
            throw FilterException("Unknown filters!");
        }
    }
}

ImageBMP Parser::DoFilters(ImageBMP& image) {
    ImageBMP new_image = image;
    for (size_t i = 0; i < filters_.size(); ++i) {
        new_image = filters_[i]->Apply(new_image);
    }
    return new_image;
}
Parser::~Parser() {
    for (size_t i = 0; i < filters_.size(); ++i) {
        delete filters_[i];
    }
}
