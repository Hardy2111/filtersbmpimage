#include "ReaderAndWriter.h"

size_t CalcPad(size_t width){
    return (4 - (width * 3) % 4) % 4;
}

ReaderAndWriter::ReaderAndWriter(ImageBMP &image, std::ifstream &stream) {
    if (!stream.is_open()) {
        throw ImageException("Bad path of BMP stream!");
    }
    stream.read(reinterpret_cast<char *>(&image.BM_), sizeof(image.BM_));
    stream.seekg(0x0);
    stream.read(image.header_, sizeof(image.header_));
    if (image.BM_ != BM_IN_SIZE_T) {
        throw ImageException("Bad BMP file!");
    }
    stream.seekg(SIZEBMP_OFFSET);
    stream.read(reinterpret_cast<char *>(&image.sizebmp_), sizeof(image.sizebmp_));
    stream.seekg(WEIGHT_OFFSET);
    stream.read(reinterpret_cast<char *>(&image.width_), sizeof(image.width_));
    stream.read(reinterpret_cast<char *>(&image.height_), sizeof(image.height_));
    image.pixels_.resize(image.height_);
    char pixels_rows[3 * image.width_];
    char pad[CalcPad(image.width_)];
    stream.seekg(PIXELS_OFFSET);
    for (size_t i = 0; i < image.height_; ++i) {
        stream.read(pixels_rows, sizeof(pixels_rows));
        for (size_t j = 0; j < 3 * image.width_; j += 3) {
            image.pixels_[i].push_back({.Red = double(uint8_t (pixels_rows[j + 2])),
                                        .Green = double (uint8_t (pixels_rows[j + 1])),
                                        .Blue = double (uint8_t(pixels_rows[j]))});
        }
        stream.read(pad, sizeof(pad));
    }
}

void ReaderAndWriter::Write(ImageBMP &image, std::ofstream &stream) {
    stream.write(image.header_, PIXELS_OFFSET);
    for (size_t i = 0; i < image.height_; ++i) {
        for (size_t j = 0; j < image.width_; ++j) {
            uint8_t red = uint8_t(image.pixels_[i][j].Blue);
            uint8_t green = uint8_t(image.pixels_[i][j].Green);
            uint8_t blue = uint8_t(image.pixels_[i][j].Red);
            stream.write(reinterpret_cast<char *>(&red), 1);
            stream.write(reinterpret_cast<char *>(&green), 1);
            stream.write(reinterpret_cast<char *>(&blue), 1);
        }
        uint8_t padding = 0;
        for (size_t pad = 0; pad < CalcPad(image.width_); ++pad) {
            stream.write(reinterpret_cast<char *>(&padding), 1);
        }
    }
}
