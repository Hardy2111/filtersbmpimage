#include <catch.hpp>
#include "iostream"
#include "ImageBMP.h"
#include "Parser.h"
#include "ReaderAndWriter.h"

char cmake_build_file[] = "./image_processor";
char inp_ifstream[] = "../projects/image_processor/examples/example.bmp";
char inp_ofstream[] = "../projects/image_processor/examples/examp.bmp";

void LikeMain(int argc, char** argv){
    Parser parser(argc, argv);
    ImageBMP image_bmp;
    ReaderAndWriter readerandwriter(image_bmp, parser.istream);
    image_bmp = parser.DoFilters(image_bmp);
    readerandwriter.Write(image_bmp, parser.ostream);
}

TEST_CASE("Cust Exception") {
    char filter[] = "-cust";
    char cust_equal[] = "threshold";
    int argc = 5;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter, cust_equal};
    REQUIRE_THROWS_WITH( Parser(argc, argv),std::string("Bad equal for -cust filter!"));
}

TEST_CASE("Blur Exception") {
    char filter[] = "-blur";
    char cust_equal[] = "threshold";
    int argc = 5;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter, cust_equal};
    REQUIRE_THROWS_WITH( Parser(argc, argv),std::string("Bad sigma for -blur filter!"));
}

TEST_CASE("Edge Exception") {
    char filter[] = "-edge";
    char cust_equal[] = "threshold";
    int argc = 5;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter, cust_equal};
    REQUIRE_THROWS_WITH( Parser(argc, argv),std::string("Bad threshold for -edge filter!"));
}

TEST_CASE("Crop Exception") {
    char filter[] = "-crop";
    char wight[] = "800";
    char height[] = "-edge";
    int argc = 6;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter, wight, height};
    REQUIRE_THROWS_WITH(LikeMain(argc, argv), std::string("Bad height and width for -crop filter!"));
}

TEST_CASE("File path Exception") {
    char bad_inp_ifstream[] = "../projects/image_processor/examples/exmaple.bbb";
    char filter[] = "-crop";
    char wight[] = "800";
    char height[] = "900";
    int argc = 6;
    char* argv[]{cmake_build_file, bad_inp_ifstream, inp_ofstream, filter, wight, height};
    REQUIRE_THROWS_WITH(LikeMain(argc, argv), std::string("Bad path of BMP stream!"));
}

TEST_CASE("Bad BMP Exception") {
    char bad_inp_ifstream[] = "../projects/image_processor/examples/example.jpeg";
    char filter[] = "-crop";
    char wight[] = "800";
    char height[] = "900";
    int argc = 6;
    char* argv[]{cmake_build_file, bad_inp_ifstream, inp_ofstream, filter, wight, height};
    REQUIRE_THROWS_WITH(LikeMain(argc, argv), "Bad BMP file!");
}

TEST_CASE("Count arguments Exception") {
    int argc = 3;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream};
    REQUIRE_THROWS_WITH( Parser(argc, argv),std::string("Not enough arguments!"));
}

TEST_CASE("Two applications of -crop") {
    char filter_1[] = "-crop";
    char wight_1[] = "1920";
    char height_1[] = "1280";
    char filter_2[] = "-crop";
    char wight_2[] = "800";
    char height_2[] = "900";
    int argc = 9;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter_1, wight_1,
                 height_1,         filter_2,     wight_2,      height_2};
    Parser parser(argc, argv);
    ImageBMP image_bmp;
    ReaderAndWriter readerandwriter(image_bmp, parser.istream);
    image_bmp = parser.DoFilters(image_bmp);
    readerandwriter.Write(image_bmp, parser.ostream);
    REQUIRE(image_bmp.width_ == 800);
    REQUIRE(image_bmp.height_ == 900);
}


TEST_CASE("Do all filters") {
    char filter_1[] = "-crop";
    char wight[] = "1920";
    char height[] = "1280";
    char filter_2[] = "-edge";
    char threshold[] = "10";
    char filter_3[] = "-blur";
    char sigma[] = "3";
    char filter_4[] = "-gs";
    char filter_5[] = "-neg";
    char filter_6[] = "-sharp";
    char filter_7[] = "-cust";
    char equal[] = "50";
    int argc = 15;
    char* argv[]{cmake_build_file, inp_ifstream, inp_ofstream, filter_1, wight,    height,   filter_2, threshold,
                 filter_3,         sigma,        filter_4,     filter_5, filter_6, filter_7, equal};
    REQUIRE_NOTHROW(LikeMain(argc, argv));
}
